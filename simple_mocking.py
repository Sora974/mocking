"""
    Simple mocking examples
"""
from __future__ import print_function, unicode_literals

import copy
import json
from unittest import TestCase

from mock import Mock, mock_open, patch
from nose.plugins.attrib import attr

from mocking.main import ConversionError, Converter
from mocking.misc import (bar, call_bar, call_myobject, MyObject,
                          partial_mocking)


class SimpleTests(TestCase):

    # @patch("patch.to.module.myfunction")
    @patch("mocking.misc.bar", Mock(return_value=-True))
    def test_mock_a_function(self):
        assert not bar()
        assert call_bar()

    @patch.object(MyObject, "my_function", Mock(return_value=True))
    def test_object_method_mocking(self):
        """
            Object/Class method mocking
        """
        # In this case, the `MyObject.my_function` method will be overriden
        # without affecting the rest of it
        # WARNING: This will change the method in every single file!
        my_obj = MyObject()
        assert my_obj.foo() == "FOO"
        assert my_obj.my_function()

    def test_partial_mocking(self):
        """
            We keep the original behaviour of the module but we actually
            replace it with a mock that does the same thing
            This allows us to "spy" on any action it was subject to
        """
        with patch("mocking.misc.json", Mock(wraps=json)) as m_json:
            m_json.loads.return_value = ["mocked"]
            json_str = partial_mocking()
            assert json_str == "[\"mocked\"]"


class TestConverter(TestCase):

    def load_test_data(self, filepath="test.json"):
        with open(filepath) as json_file:
            return json_file.read()

    @patch("mocking.main.open", create=True)
    def test_update_currency(self, m_open):
        test_data = self.load_test_data()
        mock_open(m_open, read_data=test_data)
        expected_dict = {
            "EUR": {
                "EUR": 1.0,
                "GBP": 1.5,
                "USD": 1.3
            },
            "GBP": {
                "EUR": 1.2,
                "GBP": 1.0,
                "USD": 1.5
            },
            "USD": {
                "EUR": 1.2,
                "GBP": 1.6,
                "USD": 1.0
            }
        }
        converter = Converter()
        m_open.assert_called_once_with(converter.currency_filepath)
        self.assertEqual(converter.conversion_dict, expected_dict)

    @attr("integration")
    def test_update_conversion_dict(self):
        with patch("mocking.main.open", create=True) as m_open:
            mock_open(m_open, read_data=self.load_test_data())
            converter = Converter()
            old_dict = copy.deepcopy(converter.conversion_dict)
            converter.update_conversion_rates()
            self.assertNotEqual(old_dict, converter.conversion_dict)

    @patch("mocking.main.open", create=True)
    @patch("mocking.main.requests")
    def test_save_conversion_dict(self, m_requests, m_open):
        mock_open(m_open, read_data=self.load_test_data())
        converter = Converter()
        converter.update_conversion_rates()
        converter.save()
        self.assertEqual(9, m_requests.get.call_count)
        uri = "http://download.finance.yahoo.com/d/quotes?s=EURUSD=X&f=sl1"
        m_requests.get.assert_any_call(uri)

    @patch.object(Converter, "load_data", Mock(return_value={}))
    def test_example_mock_method(self):
        """
            Mock the load_data to raise the ConversionError expection
        """
        converter = Converter()
        self.assertRaises(ConversionError, converter.convert, 10, "EUR", "USD")
