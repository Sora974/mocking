Run the tests
=============

Using nose

$ pip install nose
$ nosetests simple_mocking.py

# Ran 10 tests in 2.657s

To avoid running integration tests use:

$ nosetests simple_mocking.py -A "not integration"

# Ran 9 tests in 0.029s


