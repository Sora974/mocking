"""
    Simple code to cover all possible mockings
"""
from __future__ import print_function, unicode_literals
import json
import requests
import settings
from urlparse import urljoin


class ConversionError(Exception):
    pass


class Converter(object):

    endpoint_uri = urljoin(settings.BASE_URI, "/d/quotes?s={}{}=X&f=sl1")
    currency_filepath = "currency.json"

    def load_data(self):
        with open(self.currency_filepath) as json_file:
            return json.load(json_file)

    def __init__(self):
        self.conversion_dict = self.load_data()

    def get_uri(self, source_currency, destination_currency):
        return self.endpoint_uri.format(source_currency, destination_currency)

    def update_currency_rates(self, currency_code, currency_dict):
        for dest_code, value in currency_dict.iteritems():
            uri = self.get_uri(currency_code, dest_code)
            response = requests.get(uri)
            conversion_rate = float(response.text.strip().split(',', 1)[-1])
            self.conversion_dict[currency_code][dest_code] = conversion_rate

    def update_conversion_rates(self):
        for code, rate_dict in self.conversion_dict.iteritems():
            self.update_currency_rates(code, rate_dict)

    def show_rates(self, currency):
        for dest_currency, rate in self.conversion_dict[currency].iteritems():
            print("Rate [{} => {}] = {}".format(currency, dest_currency, rate))

    def convert(self, amount, source_code, dest_code):
        try:
            return amount * self.conversion_dict[source_code][dest_code]
        except KeyError:
            raise ConversionError("No DB entry to convert this rate...")

    def save(self):
        with open(self.currency_filepath, "w") as out_file:
            json.dump(self.conversion_dict, out_file)


if __name__ == '__main__':
    converter = Converter()
    converter.show_rates("EUR")
    print(converter.convert(amount=10.5, source_code="USD", dest_code="GBP"))
    print("--- After update ---")
    converter.update_conversion_rates()
    converter.show_rates("EUR")
    print(converter.convert(amount=10.5, source_code="USD", dest_code="GBP"))

    print("Saving...")
    converter.save()
