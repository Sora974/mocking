from mocking import settings
import json

def print_settings(settings):
    print settings


def bar():
    return False


def call_bar():
    return bar()


def call_myobject():
    my_obj = MyObject()
    return my_obj.my_function()


def partial_mocking():
    json_data = json.loads("{}")
    return json.dumps(json_data)


class MyObject(object):

    def foo(self):
        return "FOO"

    def my_function(self):
        return False
